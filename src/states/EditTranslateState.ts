import StateInterface from './StateInterface';
import ServicePool from "../services/ServicePool";

class EditTranslateState implements StateInterface{

    public name:string;

    constructor() {
        this.name = "edittranslatestate";
    }

    public enter(data:any){
        var obj = data.object;
        var controller = data.controller;
        var oldposition  = obj.position.clone();

        var sceneservice = ServicePool.getInstance().getService("sceneservice");
        var statemachine = ServicePool.getInstance().getService("statemachine");
        if(sceneservice) {
            sceneservice.registerEvent("mousemove",this.name,function(e:any,_data:any){
                obj.setPosition(oldposition.add(e.accum));
                controller.setPosition(oldposition.add(e.accum));
            });
            sceneservice.registerEvent("mouseup",this.name,function(_e:any,_data:any){
                statemachine.popState();
            });
        }
    }

    public update(){}

    public leave(){
        var sceneservice = ServicePool.getInstance().getService("sceneservice");
        if(sceneservice) {
            sceneservice.removeEvent("mousemove",this.name);
            sceneservice.removeEvent("mouseup",this.name);
        }

    }


}

export default EditTranslateState;