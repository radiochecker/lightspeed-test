import StateInterface from './StateInterface';
import ServicePool from "../services/ServicePool";

class EditRotateState implements StateInterface{

    public name:string;

    constructor() {
        this.name = "editrotatestate";
    }

    public enter(obj:any){

        var sceneservice = ServicePool.getInstance().getService("sceneservice");
        var statemachine = ServicePool.getInstance().getService("statemachine");

        if(sceneservice) {
            sceneservice.registerEvent("mousemove",this.name,function(e:any,_data:any){
                obj.rotate(e.delta.x);
            });
            sceneservice.registerEvent("mouseup",this.name,function(_e:any,_data:any){
                statemachine.popState();
            });
        }
    }

    public update(){}

    public leave(){
        var sceneservice = ServicePool.getInstance().getService("sceneservice");
        if(sceneservice) {
            sceneservice.removeEvent("mousemove",this.name);
            sceneservice.removeEvent("mouseup",this.name);
        }
    }


}

export default EditRotateState;