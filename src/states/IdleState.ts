import StateInterface from './StateInterface';
import ServicePool from "../services/ServicePool";

class IdleState implements StateInterface{
    public name:string;
    private shapename:string="";
    private lastobj:any = null;
    constructor() {
        this.name = "idlestate";
        var self = this;
        var el = document.getElementById("shapecontainer");
        if(el){
            var lasttarget:any = null;
            el.addEventListener('click',function(e:any){
                if(e.target && e.target.id){
                    if(lasttarget){
                        lasttarget.className = "";
                    }
                    if(lasttarget == e.target){
                        self.shapename = "";
                    }else{
                        self.shapename = e.target.id;
                        e.target.className = "selected";
                    }
                    lasttarget = e.target;
                }
            },false);
        }

    }

    public enter(){
        var sceneservice = ServicePool.getInstance().getService("sceneservice");
        var statemachine = ServicePool.getInstance().getService("statemachine");
        var self = this;
        if(sceneservice) {
            sceneservice.registerEvent("click",this.name,function(e:any,obj:any){
                if(statemachine && obj !== null) {
                    statemachine.changeState("editstate",obj);
                }else{
                    sceneservice.deserialize([{type:self.shapename,position:e.pos.serialize()}]);
                }
            });
            sceneservice.registerEvent("mousemove",this.name,function(_e:any,obj:any){
                if(self.lastobj){
                    self.lastobj.highlight = false;
                }
                if(obj){
                    obj.highlight = true;
                }
                self.lastobj = obj;
            });
        }
    }

    public update(){}

    public leave(){
        var sceneservice = ServicePool.getInstance().getService("sceneservice");

        if(sceneservice) {
            sceneservice.removeEvent("click",this.name);
            sceneservice.removeEvent("mousemove",this.name);
        }
        if(this.lastobj){
            this.lastobj.highlight = false;
        }
    }
}

export default IdleState;