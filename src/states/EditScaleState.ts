import StateInterface from './StateInterface';
import ServicePool from "../services/ServicePool";

class EditScaleState implements StateInterface{

    public name:string;

    constructor() {
        this.name = "editscalestate";
    }

    public enter(obj:any){
        var sceneservice = ServicePool.getInstance().getService("sceneservice");
        var statemachine = ServicePool.getInstance().getService("statemachine");
        var oldscale = obj.scale;
        if(sceneservice) {
            sceneservice.registerEvent("mousemove",this.name,function(e:any,_data:any){
                obj.scale = oldscale * (e.pos.distance(obj.position)/100);
            });
            sceneservice.registerEvent("mouseup",this.name,function(_e:any,_data:any){
                statemachine.popState();
            });
        }
    }

    public update(){}

    public leave(){
        var sceneservice = ServicePool.getInstance().getService("sceneservice");
        if(sceneservice) {
            sceneservice.removeEvent("mousemove",this.name);
            sceneservice.removeEvent("mouseup",this.name);
        }
    }

}

export default EditScaleState;