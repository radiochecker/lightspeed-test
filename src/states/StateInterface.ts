interface StateInterface {
    name:string;
    enter:any;
    leave:any;
    update:any;
}
export default StateInterface;