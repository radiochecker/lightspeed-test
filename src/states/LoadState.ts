import StateInterface from './StateInterface';
import ServicePool from "../services/ServicePool";

class LoadState implements StateInterface {
    public name:string;

    constructor() {
      this.name = "loadstate";
    }

    public enter(){}

    public update(){
        var itemstring = window.localStorage.getItem("scene");

        var sceneservice = ServicePool.getInstance().getService("sceneservice");
        if(itemstring && sceneservice){
            sceneservice.deserialize(JSON.parse(itemstring));   
        }
        ServicePool.getInstance().getService("statemachine").changeState("idlestate");
    }

    public leave(){}

}

export default LoadState;