import StateInterface from './StateInterface';
import ServicePool from "../services/ServicePool";

class SaveState implements StateInterface {
    public name:string;

    constructor() {
      this.name = "savestate";
    }

    public enter(){}

    public update(){
        var sceneservice = ServicePool.getInstance().getService("sceneservice");
        if(sceneservice){
            var save = sceneservice.serialize();
            window.localStorage.setItem("scene",JSON.stringify(save));
            ServicePool.getInstance().getService("statemachine").changeState("idlestate");
        }
    }

    public leave(){}

}

export default SaveState;