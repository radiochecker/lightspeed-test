import StateInterface from './StateInterface';
import ServicePool from "../services/ServicePool";
import {Control , HOTREGION} from "../models/Control";

class EditState implements StateInterface{
    public name:string;
    private object:any;
    private controller:Control = new Control();

    constructor() {
        this.name = "editstate";
        this.controller.visible = false;
        var sceneservice = ServicePool.getInstance().getService("sceneservice");
        if(sceneservice) {
            sceneservice.addUI(this.controller);
        }
    }

    public enter(obj:any){
        this.object = obj;
        this.controller.visible = true;

        this.controller.setPosition(this.object.position);

        var sceneservice = ServicePool.getInstance().getService("sceneservice");
        var statemachine = ServicePool.getInstance().getService("statemachine");

        var self = this;
        if(sceneservice) {
            sceneservice.registerEvent("click",this.name,function(_e:any,obj:any){
                if(obj == null){
                    statemachine.changeState("savestate");
                }else{
                    self.object = obj;
                    self.controller.setPosition(obj.position);
                }
            });
            sceneservice.registerEvent("mousedown",this.name,function(e:any,_data:any){
                var region = self.controller.regionhit(e.pos);
                switch(region){
                    case HOTREGION.TRANSLATE: statemachine.pushState("edittranslatestate",{object:self.object,controller:self.controller});return;
                    case HOTREGION.ROTATE: statemachine.pushState("editrotatestate",self.object);return;
                    case HOTREGION.SCALE: statemachine.pushState("editscalestate",self.object);return;
                }

            });
            sceneservice.registerEvent('keydown',this.name, function(event:any){
                if(event.keyCode == 8 || event.keyCode == 46){
                    sceneservice.delete(self.object);
                    statemachine.changeState("savestate");
                }
            });
        }
    }

    public update(){}

    public leave(){

        var sceneservice = ServicePool.getInstance().getService("sceneservice");
        if(sceneservice) {
            sceneservice.removeEvent("click",this.name);
            sceneservice.removeEvent("mousedown",this.name);
            sceneservice.removeEvent("keydown",this.name);
        }

        this.controller.visible = false;

    }

}

export default EditState;