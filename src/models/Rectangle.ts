import Geometry from "./Geometry";

class Rectangle extends Geometry{

    constructor() {
        super("rectangle");
    }

    protected render(ctx : any) {
        ctx.fillRect(- this.size.x/2, - this.size.y/2, this.size.x, this.size.y);
        if(this.highlight) {
            ctx.strokeStyle = this.highlightcolor;
            ctx.strokeRect(- this.size.x/2, - this.size.y/2, this.size.x, this.size.y);
        }
    }
}

export default Rectangle;