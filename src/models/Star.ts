import Geometry from "./Geometry";

class Star extends Geometry{

    constructor() {
        super("star");
    }

    //https://stackoverflow.com/questions/25837158/how-to-draw-a-star-by-using-canvas-html5
    private drawStar(ctx:any , cx:number, cy:number, spikes:number ,outerRadius:number, innerRadius:number){
        var rot=Math.PI/2*3;
        var x=cx;
        var y=cy;
        var step=Math.PI/spikes;

        ctx.beginPath();
        ctx.moveTo(cx,cy-outerRadius);
        for(var i=0;i<spikes;i++){
            x=cx+Math.cos(rot)*outerRadius;
            y=cy+Math.sin(rot)*outerRadius;
            ctx.lineTo(x,y);
            rot+=step;

            x=cx+Math.cos(rot)*innerRadius;
            y=cy+Math.sin(rot)*innerRadius;
            ctx.lineTo(x,y);
            rot+=step;
        }
        ctx.lineTo(cx,cy-outerRadius);
        ctx.closePath();
        ctx.fill();

        if(this.highlight) {
            ctx.strokeStyle = this.highlightcolor;
            ctx.stroke();
        }
    }

    protected render(ctx : any) {
       return this.drawStar(ctx,0,0,5, this.size.x, this.size.x/2);
    }
}

export default Star;