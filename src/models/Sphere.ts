import Geometry from "./Geometry";

class Sphere extends Geometry{

    constructor() {
        super("sphere");
    }

    protected render(ctx : any) {
        ctx.beginPath();
        ctx.arc(0, 0, this.size.x, 0, Math.PI*2);
        ctx.fill();
        if(this.highlight){
            ctx.strokeStyle = this.highlightcolor;
            ctx.stroke();
        }
    }
}

export default Sphere;