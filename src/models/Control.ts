import Geometry from "./Geometry";
import Vector2 from "../utils/Vector2";

enum HOTREGION {
    NOTHIT = 1,
    TRANSLATE = 2,
    ROTATE = 3,
    SCALE = 4
}

class Control extends Geometry{

    constructor() {
        super("control");
    }

    public regionhit(vec2:Vector2):HOTREGION{
        var delta:Vector2 = vec2.minus(this.position);
        if((Math.abs(delta.x) < 10  && delta.y < 0 && delta.y > -100)|| (Math.abs(delta.y) < 10 && delta.x > 0 && delta.x < 100)){
            return HOTREGION.TRANSLATE;
        }
        if(delta.x > 80 && delta.y < -80 && delta.x < 100 && delta.y > -100 ){
            return HOTREGION.SCALE;
        }
        if(delta.length() < 50){
            return HOTREGION.ROTATE;
        }
        return HOTREGION.NOTHIT;
    }

    protected render(ctx : any) {
        ctx.beginPath();
        ctx.strokeStyle = '#ff0000';
        ctx.moveTo(0, 0);ctx.lineTo(0, -100);
        ctx.lineTo(0, 0);ctx.lineTo(100, 0);
        ctx.stroke();

        ctx.beginPath();
        ctx.arc(0, -100, 3, 0, Math.PI*2);
        ctx.arc(100, 0, 3, 0, Math.PI*2);
        ctx.fillStyle = "#FF0000";
        ctx.fill();

        ctx.beginPath();
        ctx.arc(0, 0, 50, 0, -Math.PI/2);
        ctx.strokeStyle = '#00ff00';
        ctx.stroke();

        ctx.fillStyle = "#0000ff";
        ctx.fillRect(80, - 100, 20, 20);
    }
}

export {Control,HOTREGION};