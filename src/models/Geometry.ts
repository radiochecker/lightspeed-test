import Vector2 from "../utils/Vector2";

class Geometry {

    protected position:Vector2;
    protected rotation:number;
    protected scale:number;
    protected size:Vector2;

    readonly type:string;

    public visible:boolean = true;
    public highlight:boolean = false;

    protected highlightcolor:string = '#ff0000';

    constructor(type:string) {
        this.position = new Vector2(0,0);
        this.size = new Vector2(40,40);
        this.rotation = 0;
        this.scale = 1.0;
        this.type = type;
    }

    protected render(_ctx : any) { }

    public setPosition(vec2:Vector2){
        this.position = vec2.clone();
    }

    public setRotation(rot:number){
        this.rotation = rot;
    }

    public setScale(scale:number){
        this.scale = scale;
    }

    public rotate(rot:number){
        this.rotation += rot;
    }

    public hit(vec2:Vector2):boolean{
        if(this.position.distance(vec2) < this.size.length()*this.scale/2){
            return true;
        }
        return false;
    }

    public serialize() {
        var desc = {} as any;
        desc.type = this.type;
        desc.position = this.position.serialize();
        desc.rotation = this.rotation;
        desc.scale = this.scale;
        return desc;
    }


    public deserialize(desc:any){
        if(desc.position && desc.position.length == 2){
            this.position = new Vector2(0,0);
            this.position.deserialize(desc.position);
        }
        if(desc.rotation ){
            this.setRotation(desc.rotation);
        }
        if(desc.scale){
            this.setScale(desc.scale);
        }
    }

    public draw(ctx : any) {
        if(this.visible === false)
            return;
        ctx.save();
        ctx.translate( this.position.x, this.position.y );
        ctx.scale(this.scale,this.scale);
        ctx.rotate(this.rotation*Math.PI/180);
        this.render(ctx);
        ctx.restore();
    }
}

export default Geometry;