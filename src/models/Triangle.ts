import Geometry from "./Geometry";

class Triangle extends Geometry{

    constructor() {
        super("triangle");
    }

    protected render(ctx : any) {
        ctx.beginPath();
        ctx.moveTo(0 , 0 - this.size.y/2);
        ctx.lineTo(0 - this.size.x/2, 0 + this.size.y/3);
        ctx.lineTo(0 + this.size.x/2, 0 + this.size.y/3);
        ctx.lineTo(0 , 0 - this.size.y/2);
        ctx.fill();
        if(this.highlight) {
            ctx.strokeStyle = this.highlightcolor;
            ctx.stroke();
        }
    }
}

export default Triangle;