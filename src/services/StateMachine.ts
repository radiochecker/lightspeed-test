import StateInterface from "../states/StateInterface";
import ServicePool from "./ServicePool";
class StateMachine {

    private static _instance: StateMachine = new StateMachine();

    private stateMap:any;
    private statestack:Array<any>;

    private cmdpop:boolean;
    private pushstate:any;
    private pushstatedata:any;

    constructor() {
        if (StateMachine._instance) {
            throw new Error("Error: Instantiation failed: Use StateMachine.getInstance() instead of new.");
        }
        StateMachine._instance = this;
        ServicePool.getInstance().registerService("statemachine",this);
        this.stateMap = {};
        this.statestack = [];

        this.cmdpop = false;
        this.pushstate = null;
        this.pushstatedata = null;
    }


    public static getInstance(): StateMachine {
        return StateMachine._instance;
    }

    public addState(state:StateInterface){
        this.stateMap[state.name] =  state;
    }

    public changeState(name:string,data:any = null){
        if(!this.cmdpop && this.stateMap[name]){
           this.popState();
           this.pushState(name,data);
        }
    }

    public pushState(name:string,data:any = null){
        if(this.stateMap[name]){
            this.pushstate = this.stateMap[name];
            this.pushstatedata = data;
        }
    }

    public update(){

        if(this.statestack.length > 0){
            this.statestack[this.statestack.length - 1].update();
        }
        if(this.cmdpop){
            var state = this.statestack.pop();
            if(state){
                state.leave();
            }
            this.cmdpop = false;
        }
        if(this.pushstate){
            this.pushstate.enter(this.pushstatedata);
            this.statestack.push( this.pushstate);
            this.pushstate = null;
        }
    }

    public popState(){
        this.cmdpop = true;
    }
}

export default StateMachine;