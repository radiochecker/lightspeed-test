import ServicePool from "./ServicePool";

import Geometry from '../models/Geometry';
import Rectangle from '../models/Rectangle';
import Sphere from '../models/Sphere';
import Star from '../models/Star';
import Triangle from '../models/Triangle';

import Vector2 from '../utils/Vector2';

class SceneService {

    private static _instance: SceneService = new SceneService();

    private objects:Array<any>;
    private uis:Array<Geometry>;
    private canvas:any = null;
    private eventMap:any = {};

    constructor() {
        if (SceneService._instance) {
            throw new Error("Error: Instantiation failed: Use SceneService.getInstance() instead of new.");
        }
        SceneService._instance = this;
        ServicePool.getInstance().registerService("sceneservice",this);
        this.objects = [];
        this.uis = [];
    }

    public static getInstance(): SceneService {
        return SceneService._instance;
    }

    public initalize(){
        var canvas = document.createElement('canvas');

        canvas.id = "scenelayer";
        canvas.width = 1024;
        canvas.height = 768;
        canvas.style.position = "absolute";
        canvas.style.border = "1px solid";
        var self = this;

        var lastposition = new Vector2(0,0);
        var startposition = new Vector2(0,0);
        canvas.addEventListener('click', function(e:any) {
            e.pos = new Vector2(e.offsetX,e.offsetY);
            self.handleEvent("click",e);
        }, false);

        canvas.addEventListener('mousemove', function(e:any) {
            var newpos = new Vector2(e.offsetX,e.offsetY);
            e.delta = newpos.minus(lastposition);
            e.accum = newpos.minus(startposition);
            e.pos = newpos;
            self.handleEvent("mousemove",e);
            lastposition = newpos;
        }, false);

        canvas.addEventListener('mouseup', function(e:any) {
            e.pos = new Vector2(e.offsetX,e.offsetY);
            self.handleEvent("mouseup",e);
        }, false);

        canvas.addEventListener('mousedown', function(e:any) {
            lastposition = new Vector2(e.offsetX,e.offsetY);
            startposition = lastposition.clone();
            e.pos = lastposition.clone();
            self.handleEvent("mousedown",e);
        }, false);

        document.addEventListener('keydown', function(e:any){
            self.handleEvent("keydown",e);
        },false);

        var body = document.getElementsByTagName("body")[0];
        body.appendChild(canvas);

        this.canvas = canvas;
    }

    private handleEvent(eventname:string,e:any) {
        if (e.pos) {
            for (var i = 0; i < this.objects.length; i++) {
                if (this.objects[i].hit(e.pos)) {
                    this.fireEvent(eventname, e, this.objects[i]);
                    return;
                }
            }
        }
        this.fireEvent(eventname,e);
    }

    public registerEvent(eventname:string,key:string,callback:any){
        this.eventMap[eventname] = this.eventMap[eventname] || {};
        this.eventMap[eventname][key] = callback;
    }

    public removeEvent(eventname:string,key:string){
        this.eventMap[eventname] = this.eventMap[eventname];
        if(this.eventMap[eventname]){
            this.eventMap[eventname][key] = null;
        }
    }

    private fireEvent(eventname:string,e:any,data:any=null){
        if( this.eventMap[eventname] ){
            for(var key in this.eventMap[eventname]){
                if(this.eventMap[eventname][key]){
                    this.eventMap[eventname][key](e,data);
                }
            }
        }
    }

    public serialize(){
        var descList = [];
        for(var i=0; i<this.objects.length;i++){
            descList.push(this.objects[i].serialize());
        }
        return descList;
    }

    public delete(obj:any){
        var index = -1;
        for(var i=0; i<this.objects.length;i++){
            if(obj == this.objects[i]){
                index = i;
            }
        }
        if(index > -1){
            this.objects.splice(index,1);
        }
    }

    public deserialize(list:any){
        for(var i = 0; i<list.length; i++){
            var desc = list[i];
            let obj: Geometry | null = null;
            switch(desc.type){
                case "rectangle": obj = new Rectangle(); break;
                case "triangle": obj = new Triangle(); break;
                case "sphere": obj = new Sphere(); break;
                case "star": obj = new Star();  break;
            }
            if(obj == null){
                continue;
            }
            obj.deserialize(desc);
            this.objects.push(obj);
        }
    }

    public addUI(obj:Geometry){
        this.uis.push(obj);
    }

    public update(){
        var ctx = this.canvas.getContext('2d');
        if(ctx){
            ctx.clearRect(0,0,this.canvas.width,this.canvas.height);
        }
        for(var i=0; i<this.objects.length;i++){
            this.objects[i].draw(ctx);
        }
        for(var i=0; i<this.uis.length;i++){
            this.uis[i].draw(ctx);
        }
    }

}

export default SceneService;