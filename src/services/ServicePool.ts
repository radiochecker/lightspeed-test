class ServicePool {

    private static _instance: ServicePool = new ServicePool();

    private serviceMap:any;

    constructor() {
        if (ServicePool._instance) {
            throw new Error("Error: Instantiation failed: Use ServicePool.getInstance() instead of new.");
        }
        ServicePool._instance = this;
        this.serviceMap = {};
    }


    public static getInstance(): ServicePool {
        return ServicePool._instance;
    }

    public registerService(name:string, service:any){
        this.serviceMap[name] =  service;
    }

    public update(){
        for(var key in this.serviceMap){
            this.serviceMap[key].update();
        }
    }

    public getService(name:string){
       return this.serviceMap[name];
    }

}

export default ServicePool;