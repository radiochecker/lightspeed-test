
import StateMachine from './services/StateMachine';
import SceneService from './services/SceneService';
import ServicePool from './services/ServicePool';


import EditState from './states/EditState';
import LoadState from './states/LoadState';
import SaveState from './states/SaveState';
import IdleState from './states/IdleState';
import EditTranslateState from './states/EditTranslateState';
import EditRotateState from './states/EditRotateState';
import EditScaleState from './states/EditScaleState';

class testapplication {

    constructor() {
        
    }

    start() {

        SceneService.getInstance().initalize();

        StateMachine.getInstance().addState(new EditState());
        StateMachine.getInstance().addState(new LoadState());
        StateMachine.getInstance().addState(new SaveState());
        StateMachine.getInstance().addState(new IdleState());
        StateMachine.getInstance().addState(new EditTranslateState());
        StateMachine.getInstance().addState(new EditRotateState());
        StateMachine.getInstance().addState(new EditScaleState());

        StateMachine.getInstance().changeState("loadstate");


        setInterval(function(){
            ServicePool.getInstance().update();
        },30);

    }
}

export default testapplication;