class Vector2 {
    x:number;
    y:number;
    constructor(x:number,y:number) {
        this.x = x;
        this.y = y;
    }

    public serialize() {
        return [this.x,this.y];
    }

    public deserialize(list:Array<number>) {
        this.x = list[0];
        this.y = list[1];
    }

    public minus(vec2:Vector2):Vector2 {
        return new Vector2(this.x -vec2.x,this.y-vec2.y);
    }

    public add(vec2:Vector2):Vector2 {
        return new Vector2(this.x +vec2.x,this.y+vec2.y);
    }

    public clone():Vector2 {
        return new Vector2(this.x,this.y);
    }

    public distance(vec2:Vector2):number {
        return Math.sqrt((this.x -vec2.x)*(this.x -vec2.x)+(this.y-vec2.y)*(this.y-vec2.y));
    }
    public length() :number{
        return Math.sqrt(this.x*this.x +this.y*this.y);
    }
}

export default Vector2;