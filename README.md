# Lightspeed Graphics Technical Test

This is the project skeleton for the Lightspeed Graphics technical test. It is
intended as a starting point, and you may change any part of it as needed.

Get started by installing Node.js and NPM, then running `npm install` in this
directory.

The provided NPM commands are

- `npm run build` - Builds the application and puts the result into `dist/`
- `npm run serve` - Runs a development server on `http://localhost:8080`

Instructions:
1: Select one button on the top to create shape on canvas (left click). Unselect the button to disable creation.
2: Click the shape in canvas to enter edit mode. the transform handler will show above the shape.
3: Red line in transform handler is for translation; green arc is for rotation; blue rect on right top is for scale.
4: Select the shape and press backspace to delete.
5: Click the canvas to quit the edit mode.
6: The scene will be saved after leaving edit mode.

Based on the brief of application, the challenging part here is the user interaction and object control (creation, delete, translate,rotation and scale). So the best approaching is building  state machine to handle the interaction. The best part of this architecture is the application can only be in one state where the interaction be handled and do not need to worry about the other state. Each state is in self contained structure. In terms of different interaction or data the state can be switched to another without worrying about the future state.

The application states flowing is like
https://drive.google.com/file/d/0B6BzuewBjdpNWDNkY2tTS1pEMUFtaEF1UDNxU0VkM3E0alU0/view?usp=sharing

Considering the codeline and time limit there are some parts can be improved later.  Geometry creation should be in one separate state. Ui parts should be in separate manager. Click event should be discard when mouse up is handled. Geometry creation should be configurable. Depth sorting of scene objects. 